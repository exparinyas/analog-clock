const handHour = document.querySelector('.rotate-hand-hour');
const handMin = document.querySelector('.rotate-hand-min');
const handSec = document.querySelector('.rotate-hand-sec');

// ทำเวลาแบบ Realtime โดยใช้ setInterval ทำซ้ำๆ โดยกำหนดเวลา 1 sec (1 sec = 1000 milliseconds)
setInterval(() => {
    // Get เวลาจากเครื่องคอมพิวเตอร์ โดย getHours / getMinutes / getSeconds
    let date = new Date();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();

    // นำเวลาที่ได้มาทำ Rotate ให้กับเข็มนาฬิกา
    let rotateHour = hour * 30;     // ให้ชั่วโมง rotate ทีละ 30deg ไปเรื่อยๆตามชั่วโมงที่ได้จากเครื่อง โดยเอาชั่วโมงที่ได้มาคูณกับ 30 โดย 30 นั้นมาจาก 360deg/12 (12 นั้นคือชั่วโมงทั้งหมดของนาฬิกา Analog) = 30deg จะเป็นหน่วยต่อชั่วโมง ซึ่ง 1 ชั่วโมงจะเท่ากับ 30deg
    let rotateMin = min * 6;        // ให้นาที rotate ทีละ 6deg ไปเรื่อยๆตามนาทีที่ได้จากเครื่อง โดยเอานาทีที่ได้มาคูณกับ 6 โดย 6 นั้นมาจาก 360deg/60 (60 นั้นคือนาทีทั้งหมดของนาฬิกา Analog) = 6deg จะเป็นหน่วยต่อนาที ซึ่ง 1 นาทีจะเท่ากับ 6deg 
    let rotateSec = sec * 6;        // ให้วินาที rotate ทีละ 6deg ไปเรื่อยๆตามวินาทีที่ได้จากเครื่อง โดยเอาวินาทีที่ได้มาคูณกับ 6 โดย 6 นั้นมาจาก 360deg/60 (60 นั้นคือวินาทีทั้งหมดของนาฬิกา Analog) = 6deg จะเป็นหน่วยต่อวินาที ซึ่ง 1 วินาทีจะเท่ากับ 6deg

    // Update เข็มชั่วโมงและนาที แบบเรียลไทม์
    let upDateRotateHourRealTime = rotateHour + (min/2);    // ให้เข็มชั่วโมงนั้นเคลื่อนที่ตาม นาที/2 คือ นาทีในนาฬิกาทั้งหมดมี 60 นาที นำ 60 หารอะไรบ้างจะได้ 30 ซึ่งต้องหาร 2 จะได้ 30 ค่าที่ได้คือ 30deg ซึ่งเท่ากับ 1 ชั่วโมง ซึ่งนาทีที่ได้จากเครื่องจะนำมาหาร 2 และจะได้ค่า 0/2 = 0 จนถึง 59/2 = 29.5 แล้วนำค่าที่ได้นี้บวกกับ hour * 30 จะได้เข็มชั่วโมงแบบเรียลไทม์
    let upDateRotateMinRealTime = rotateMin + (sec/10);     // ให้เข็มนาทีนั้นเคลื่อนที่ตาม วินาที/10 คือ วินาทีในนาฬิกาทั้งหมดมี 60 วินาที นำ 60 หารอะไรบ้างจะได้ 6 ซึ่งต้องหาร 10 จะได้ 6 ค่าที่ได้คือ 6deg ซึ่งเท่ากับ 1 นาที ซึ่งวินาทีที่ได้จากเครื่องจะนำมาหาร 10 และจะได้ค่า 0/10 = 0 จนถึง 59/10 = 5.9 แล้วนำค่าที่ได้นี้บวกกับ min * 6 จะได้เข็มนาทีแบบเรียลไทม์

    // กำหนด Style Transform Rotate ให้กับเข็มชั่วโมง / เข็มนาที / เข็มวินาที
    handHour.style.transform = `rotate(${upDateRotateHourRealTime}deg)`;
    handMin.style.transform = `rotate(${upDateRotateMinRealTime}deg)`;
    handSec.style.transform = `rotate(${rotateSec}deg)`;

    // console.log(`${hour} Hour : ${min} Min : ${sec} Sec`);
}, 1000);

